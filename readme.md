# Welcome to the Bethany Associates Meetup
## Topic

Git workflow

## Atendees
* Timothy Krell
* Adam Krell
* ect.

## Food
* Pizza
* Soda
* No Veggies
* Water

## Presentation
* Walk through Git guide
    * This is misspellled
* Talk about Bethany's workflow
    * Master, Development, Feature branch
    * When do I rebase?
* Undoing things in Git
    * `git revert <SHA>
    * `git commit --amend`
    * `git checkout -- <bad filename>
    * `git reset <last good SHA>` or `git reset --hard <last good SHA>`
    * `git reflog` and `git reset` or `git checkout`
* Checking out someone else's fork
* Discussion / Questions

## Resources
* [Git - the simple guide](http://rogerdudler.github.io/git-guide/)
* [How to undo almost anything with git](https://github.com/blog/2019-how-to-undo-almost-anything-with-git)
* [Another helpful link](http://google.com)